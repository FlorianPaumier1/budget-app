import { type ClassValue, clsx } from "clsx"
import { twMerge } from "tailwind-merge"

export const IsoDateFormat = 'YYYY-MM-DD'

export type selectObject = {
    label: string
    value: string
}

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}
