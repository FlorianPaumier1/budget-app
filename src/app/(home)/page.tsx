"use client"
import * as React from 'react';
import Deposits from "@/components/deposits";
import Expense from "@/components/expense";
import moment from "moment";
import {findDepositsBetweenDates, findExpensesBetweenDates, ItemViewProps} from "@/components/db/query/Items";
import {Results} from "@/components/Results";
import {SetStateAction, useEffect, useState} from "react";
import {Doughnut} from "react-chartjs-2";
import {Chart, ArcElement} from 'chart.js'
import Link from "next/link";
import {Button} from "@/components/ui/button";
import {Moment} from "moment/moment";
import {IsoDateFormat} from "@/lib/utils";

type Props = {

};

const Page = (props: Props) => {

    Chart.register(ArcElement);

    const [currentMonth, setCurrentMonth] = useState(moment().startOf('month'))
    const [expenses, setExpenses]: [ItemViewProps[], SetStateAction<any>] = useState([])
    const [deposits, setDeposits]: [ItemViewProps[], SetStateAction<any>] = useState([])
    const [amountDeposits, setAmountDeposits]: [number, SetStateAction<any>] = useState(0)
    const [amountExpenses, setAmountExpenses]: [number, SetStateAction<any>] = useState(0)

    const depositType: { [key: string]: number } = {}
    const categoryDepositColor: { [key: string]: string } = {}
    const expenseType: { [key: string]: number } = {}
    const categoryExpenseColor: { [key: string]: string } = {}

    deposits.forEach((item) => {
        if (!(item.category.name in depositType)) {
            depositType[item.category.name] = item.amount
        } else {
            depositType[item.category.name] += item.amount
        }

        if (!(item.category.name in categoryDepositColor)) {
            categoryDepositColor[item.category.name] = item.category.color;
        }
    })
    expenses.forEach((item) => {
        if (!(item.category.name in expenseType)) {
            expenseType[item.category.name] = item.amount * item.cycle.multiplicator
        } else {
            expenseType[item.category.name] += item.amount * item.cycle.multiplicator
        }

        if (!(item.category.name in categoryExpenseColor)) {
            categoryExpenseColor[item.category.name] = item.category.color;
        }
    })

    const depositChart = {
        labels: Object.keys(depositType),
        datasets: [{
            label: 'Dépôts',
            data: Object.values(depositType),
            backgroundColor: Object.values(categoryDepositColor),
            hoverOffset: 4
        }]
    };

    const expenseChart = {
        labels: Object.keys(expenseType),
        datasets: [{
            label: 'Dépôts',
            data: Object.values(expenseType),
            backgroundColor: Object.values(categoryExpenseColor),
            hoverOffset: 4
        }]
    };

    useEffect(() => {
        async function getData() {
            const expenses = (await findExpensesBetweenDates(
                currentMonth.format(IsoDateFormat),
                moment(currentMonth).endOf('month').format(IsoDateFormat)
            )).filter((item) => {
                if (item.cycle.id == 6){
                    return currentMonth.month() % 3 === 0
                }else if(item.cycle.id === 7){
                    return currentMonth.month() === 6
                }else if(item.cycle.id === 5){
                    return currentMonth.month() === 1
                }
                return true;
            })
            const deposits = (await findDepositsBetweenDates(
                currentMonth.format(IsoDateFormat),
                moment(currentMonth).endOf('month').format(IsoDateFormat)
            )).filter((item) => {
                if (item.cycle.id == 6){
                    return currentMonth.month() % 3 === 0
                } else if(item.cycle.id === 7){
                    return currentMonth.month() === 6
                }else if(item.cycle.id === 5){
                    return currentMonth.month() === 1
                }
                return true;
            })
            setDeposits(deposits)
            setExpenses(expenses)
            setAmountDeposits(deposits.reduce((acc, item) => {
                return acc + (item.amount * item.cycle.multiplicator)
            }, 0))
            setAmountExpenses(expenses.reduce((acc, item) => {
                return acc + (item.amount * item.cycle.multiplicator)
            }, 0))
        }

        getData()
    }, [currentMonth]);

    const changeCurrentMonth = (date: Moment) => {
        setCurrentMonth(date)
    }

    return (
        <section className={"flex flex-col px-4"}>
            <section>
                <Button asChild variant={'default'}><Link href={'/form'}>Ajout d'un élément</Link></Button>
            </section>
            <div className={'flex flex-row'}>
                <Deposits items={deposits}/>
                <Expense items={expenses}/>
                <Results
                    month={currentMonth}
                    deposits={amountDeposits}
                    expenses={amountExpenses}
                    changeMonth={changeCurrentMonth}
                    chartExpense={<Doughnut data={depositChart}/>}
                    chartDeposit={<Doughnut data={expenseChart}/>}
                />
            </div>
        </section>
    );
};

export default Page;
