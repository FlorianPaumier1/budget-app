// @flow
import * as React from 'react';

type LayoutProps = {
    children: React.ReactNode,
    params: object,
};

const Layout = async (props: LayoutProps) => {

    return (
        <div className="flex items-start justify-between w-full py-16">
            <div className="w-full rounded shadow modal pt-8">
                <h1>Budget</h1>
                {props.children}
            </div>
        </div>
    );
};

export default Layout;
