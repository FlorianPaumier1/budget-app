import * as React from 'react';
import {ItemForm} from "@/components/form/ItemForm";
import {findTypes} from "@/components/db/query/type";
import {findCategories} from "@/components/db/query/category";
import {findUsers} from "@/components/db/query/users";
import {findCycles} from "@/components/db/query/cycles";
import {Toaster} from "@/components/ui/toaster";

type Props = {};
const Page = async (props: Props) => {

    const types = (await findTypes()).map((type) => {
        return {label: type.name, value: type.id.toString()}
    })
    const categories = (await findCategories()).map((category) => {
        return {label: category.name, value: category.id.toString()}
    })
    const users = (await findUsers()).map((user) => {
        return {label: user.name, value: user.id.toString()}
    })
    const cycles = (await findCycles()).map((cycle) => {
        return {label: cycle.name, value: cycle.id.toString()}
    })

    return (
        <div className="flex items-start justify-between w-full py-16">
            <div className="w-full rounded shadow modal pt-8">
                <h1>Création d'un élément</h1>
                <ItemForm item={null} types={types} cycles={cycles} users={users} categories={categories}/>
                <Toaster />
            </div>
        </div>
    );
};

export default Page
