// @flow
import * as React from 'react';
import {v4} from "uuid";
import {ItemViewProps} from "@/components/db/query/Items";
import {BudgetItem} from "@/components/BudgetItem";

type Props = {
    items: ItemViewProps[]
};
const Expense = ({items}: Props) => {


    return (
        <div className="flex justify-between p-8 w-[40rem] overflow-hidden">
            <div className="w-full rounded shadow card p-4">
                <h2>Expense</h2>
                <ul className={" overflow-y-scroll scrollbar-hide"}>
                    {items.map((item) => {
                        return <BudgetItem item={item} key={v4()} type={'expense'}/>
                    })}
                </ul>
            </div>
        </div>
    );
};
export default Expense;
