export const CheckmarkIcon = () => <div aria-hidden className="checkmark" />;
export const ErrorIcon = () => <div aria-hidden className="error" />;
