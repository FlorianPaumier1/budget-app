"use server"
import {prisma} from "@/components/db/prisma";

export const findCategories = async () => {
    return await prisma.category.findMany({
        select: {
            name: true,
            id: true
        }
    })
}
