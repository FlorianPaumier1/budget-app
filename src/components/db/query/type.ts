"use server"
import {prisma} from "@/components/db/prisma";

export const findTypes = async () => {
    return await prisma.type.findMany({
        select: {
            name: true,
            id: true
        }
    })
}
