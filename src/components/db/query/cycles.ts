"use server"

import {prisma} from "@/components/db/prisma";

export const findCycles = async () => {
    return prisma.cycle.findMany({
        select: {
            id: true,
            name: true
        }
    })
}
