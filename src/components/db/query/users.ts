"use server"

import {prisma} from "@/components/db/prisma";

export const findUsers = async () => {
    return prisma.user.findMany({
        select: {
            id: true,
            name: true
        }
    })
}
