"use server"
import {prisma} from "@/components/db/prisma";
import {Moment} from "moment/moment";
import moment from "moment";
import * as z from "zod";


export type ItemViewProps = {
    name: string,
    amount: number,
    date: String,
    isFixed: boolean,
    category: {
        name: string,
        color: string
    },
    cycle: {
        multiplicator: number,
    }
};

export const findDepositsBetweenDates = async (startDate: string, endDate: string) => {

    const start = moment(startDate).set({hour: 0, minute: 0, second: 0, millisecond: 0})
    const end = moment(endDate).set({hour: 0, minute: 0, second: 0, millisecond: 0})

    return await prisma.item.findMany({
        select: {
            name: true,
            amount: true,
            isFixed: true,
            startDate: false,
            endDate: false,
            category: {
                select: {
                    name: true,
                    color: true
                }
            },
            cycle: {
                select: {
                    id: true,
                    multiplicator: true
                }
            }
        },
        where: {
            AND: {
                type: {
                    name: 'deposit'
                },
                OR: [
                    {

                        startDate: {
                            lte: end.toDate()
                        },
                        endDate: {
                            gte: start.toDate()
                        }
                    },
                    {
                        startDate: {
                            lte: end.toDate()
                        },
                        endDate: null
                    }
                ]
            }
        }
    })
}

export const findExpensesBetweenDates = async (startDate: string, endDate: string) => {

    const start = moment(startDate).set({hour: 0, minute: 0, second: 0, millisecond: 0})
    const end = moment(endDate).set({hour: 0, minute: 0, second: 0, millisecond: 0})

    return await prisma.item.findMany({
        select: {
            name: true,
            amount: true,
            isFixed: true,
            startDate: false,
            endDate: false,
            category: {
                select: {
                    name: true,
                    color: true
                }
            },
            cycle: {
                select: {
                    id: true,
                    multiplicator: true
                }
            }
        },
        where: {
            AND: {
                type: {
                    name: 'expense'
                },
                OR: [
                    {

                        startDate: {
                            lte: end.toDate()
                        },
                        endDate: {
                            gte: start.toDate()
                        }
                    },
                    {
                        startDate: {
                            lte: end.toDate()
                        },
                        endDate: null
                    }
                ]
            }
        }
    })
}

export const createItem = async (item: any) => {
    if (item.id) {
        return await prisma.item.update({
            where: {
                id: item.id
            },
            data: item
        })
    } else {
        return await prisma.item.create({
            data: item
        })
    }
}
