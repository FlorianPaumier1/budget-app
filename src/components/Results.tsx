"use client"
import * as React from 'react';
import {ReactElement, SetStateAction, useEffect, useState} from "react";
import {Moment} from "moment/moment";
import moment from "moment";
import {Button} from "@/components/ui/button";

type Props = {
    expenses: number,
    deposits: number,
    month: Moment,
    changeMonth: SetStateAction<any>,
    chartExpense: ReactElement,
    chartDeposit: ReactElement,
};

export const Results = ({expenses, deposits, month, changeMonth, chartExpense,chartDeposit}: Props) => {
        const [restant, setRestant] = useState(0)

        useEffect(() => {
            setRestant(deposits - expenses)
        }, [expenses, deposits, month])

        return (
            <div className="flex justify-between p-16 w-[48rem] overflow-hidden">
                <div className="w-full rounded shadow card p-4">
                    <h2>Résultats pour le mois : {month.format('MMMM')}</h2>
                    <section className={"flex flex-row justify-center"}>
                        <input defaultValue={month.format('YYYY-MM')} type={"month"} onChange={(e) => changeMonth(moment(e.target.value))} />
                    </section>
                    <section className={"px-4"}>
                        <div className={"flex flex-row justify-between"}>

                    <span className={"text-lg flex flex-col"}>
                        <span>
                            Total dépenses
                        </span>
                        <span className={"text-red-500"}>
                            - {expenses.toFixed(2)}
                            </span>
                    </span>
                            <span className={"text-lg flex flex-col"}>
                            <span>
                                Total Dépots
                            </span>
                            <span className={"text-green-500"}>
                        + {deposits.toFixed(2)}
                            </span>
                    </span>
                            <span className={"text-lg flex flex-col"}>
                            <span>
                                Restant
                            </span>
                            <span className={restant < 0 ? "text-red-500" : "text-green-500"}>
                        {restant.toFixed(2)}
                            </span>
                        </span>
                        </div>
                        {chartExpense}
                        {chartDeposit}
                    </section>
                </div>
            </div>
        );
    }
;
