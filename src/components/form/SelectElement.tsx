import * as React from "react"

import {Select, SelectContent, SelectItem, SelectTrigger, SelectValue} from "@/components/ui/select";
import {ControllerRenderProps} from "react-hook-form";
import {v4} from "uuid";
import {SelectGroup, SelectLabel} from "@radix-ui/react-select";

type Props = {
    values: { value: string, label: string }[],
    label: string,
    field: ControllerRenderProps<any, any>,
    placeholder?: string,
    required?: boolean
}

export function SelectElement({values = [], label = "", field,required = false, placeholder = "Choisir un élément"}: Props) {
    return (
        <SelectGroup className={"flex flex-col w-48"}>
            <Select onValueChange={field?.onChange} defaultValue={field?.value}>
                <SelectLabel>{label} {required && <span className={"text-red-500"}>*</span>}</SelectLabel>
                <SelectTrigger className={"bg-white"}>
                    <SelectValue placeholder={placeholder}/>
                </SelectTrigger>
                <SelectContent position={"popper"}>
                    {values.map((value) => {
                        return <SelectItem value={value.value} key={v4()}>{value.label}</SelectItem>
                    })}
                </SelectContent>
            </Select>
        </SelectGroup>
    )
}
