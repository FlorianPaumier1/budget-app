// @flow
import * as React from 'react';

type Props = {
    label?: string,
    type: string,
    id: string,
    name: string,
    required?: boolean,
    value?: string|number
};

export const Input = ({label, id, name, value = "", type= 'text', required = true}: Props) => {
    return (
        <div className="flex flex-col md:mr-16">
            {label && (
                <label htmlFor={id}
                       className="text-gray-800 text-sm font-bold leading-tight tracking-normal mb-2">
                    {label} {required && <span className={"text-red-500"}>*</span>}
                </label>
            )}
            <div className="relative">
                <div className="absolute right-0 text-gray-600 hover:text-gray-700 flex items-center pr-3 h-full cursor-pointer">
                    <svg className={'icon icon-tabler icon-tabler-eye'} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g id="SVGRepo_bgCarrier" strokeWidth="0"></g>
                        <g id="SVGRepo_tracerCarrier" strokeLinecap="round" strokeLinejoin="round"></g>
                        <g id="SVGRepo_iconCarrier">
                            <path
                                d="M15.7955 15.8111L21 21M18 10.5C18 14.6421 14.6421 18 10.5 18C6.35786 18 3 14.6421 3 10.5C3 6.35786 6.35786 3 10.5 3C14.6421 3 18 6.35786 18 10.5Z"
                                stroke="#000000" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"></path>
                        </g>
                    </svg>
                </div>
                <input id={id} type={type} name={name} defaultValue={value}
                       className="text-gray-600 focus:outline-none focus:border focus:border-indigo-700  bg-white
                       font-normal w-64 h-10 flex items-center pl-3 text-sm border-gray-300 rounded border shadow"
                       placeholder={label}/>
            </div>
        </div>
    );
};
