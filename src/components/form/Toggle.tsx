// @flow
import * as React from 'react';
import * as Switch from '@radix-ui/react-switch';

type Props = {
    label: string,
    value: boolean,
    name: string
};

const Toggle = ({label, value, name}: Props) => {
    return (
        <div className={"flex flex-col"}>
            <label className="Label" htmlFor="airplane-mode">
                {label}
            </label>
            <Switch.Root className="SwitchRoot" id="airplane-mode" defaultChecked={value} name={name}>
                <Switch.Thumb className="SwitchThumb" />
            </Switch.Root>
        </div>
    );
};

export default Toggle
