"use client"
import * as React from 'react';
import {Input} from "@/components/form/Input";
import {Button} from "@/components/ui/button";
import * as z from "zod"
import {useForm} from "react-hook-form";
import {zodResolver} from "@hookform/resolvers/zod";
import {Form, FormControl, FormField, FormItem} from "@/components/ui/form";
import {SelectElement} from "@/components/form/SelectElement";
import Toggle from "@/components/form/Toggle";
import {selectObject} from "@/lib/utils";
import {useToast} from "@/components/ui/use-toast";
import {v4} from "uuid";
import {useEffect} from "react";

type Props = {
    item: {
        name: string,
        amount: number,
        type?: number,
        category?: number
        id: number,
        user?: number,
        date: Date,
        cycle?: number,
        isFixed: boolean,
        startDate?: Date,
        endDate?: Date
    } | null,
    types: selectObject[],
    cycles: selectObject[],
    categories: selectObject[],
    users: selectObject[],
};


const formSchema = z.object({
    name: z.string({
        required_error: "Le nom est requis",
        invalid_type_error: "Le nom doit être une chaine de caractères",
    }).min(2, {
        message: "Le nom doit contenir au moins 2 caractères"
    }).max(50, {
        message: "Le nom doit contenir au plus 50 caractères"
    }),
    amount: z.number({
        required_error: "Le nom est requis"
    }).min(0, {
        message: "Le montant doit être supérieur à 0",
    }),
    date: z.date().nullable().optional(),
    isFixed: z.boolean(),
    startDate: z.date(),
    endDate: z.date().nullable().optional(),
    type: z.number({
        required_error: "Le type est requis"
    }).min(1, {
        message: "Le type doit être renseigné",
    }),
    category: z.number({
        required_error: "La catégorie est requis"
    }).min(1, {
        message: "La catégorie doit être renseigné",
    }),
    cycle: z.number({
        required_error: "Le cycle est requis"
    }).min(1, {
        message: "Le cycle doit être renseigné",
    }),
    user: z.number({
        required_error: "L'utilisateur nom est requis"
    }).min(1, {
        message: "L'utilisateur doit être renseigné",
    })
})

export const ItemForm = ({item = null, types, cycles, categories, users}: Props) => {

    const { toast } = useToast()

    let {
        formState,
        handleSubmit,
        ...form
    } = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema),
        defaultValues: item ?? {
            name: "",
            amount: 0,
            date: undefined,
            type: undefined,
            category: undefined,
            cycle: undefined,
            user: undefined,
            isFixed: false,
            startDate: new Date(),
            endDate: undefined
        },
    })

    function onSubmit(values: z.infer<typeof formSchema>) {
        // Do something with the form values.
        // ✅ This will be type-safe and validated.
        console.log(values)
        debugger
    }

    const showToast = (message?: string) => {
        toast({
            description: message,
            variant: "destructive"
        })
    }

    const errors = Object.values(formState.errors).map((error) => error.message)

    return (
        <Form {...form} handleSubmit={handleSubmit} formState={formState} key={v4()}>
            <form onSubmit={handleSubmit(onSubmit)} className="space-y-8 px-16 w-3/4 mx-auto">
                <section className={"flex flex-row flex-wrap gap-16 items-center py-16"}>
                    <FormField
                        control={form.control}
                        name="name"
                        render={({field}) => (
                            <FormItem>
                                <FormControl>
                                    <Input value={field.value} type={"text"} id={"name"} name={"name"} label={"Nom"}/>
                                </FormControl>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="amount"
                        render={({field}) => (
                            <FormItem>
                                <FormControl>
                                    <Input value={field.value} type={"number"} id={"amount"} name={"amount"} label={"Montant"}/>
                                </FormControl>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="date"
                        render={({field}) => (
                            <FormItem>
                                <FormControl>
                                    <Input value={field.value?.toString()} type={"date"} id={"date"} name={"date"} label={"Date"} required={false}/>
                                </FormControl>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="startDate"
                        render={({field}) => (
                            <FormItem>
                                <FormControl>
                                    <Input value={field.value?.toString()} type={"date"} id={"startDate"} name={"startDate"} label={"Date de début"}/>
                                </FormControl>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="endDate"
                        render={({field}) => (
                            <FormItem>
                                <FormControl>
                                    <Input value={field.value?.toString()} type={"date"} id={"endDate"} name={"endDate"} label={"Date de fin"} required={false}/>
                                </FormControl>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="type"
                        render={({field}) => (
                            <FormItem>
                                <SelectElement values={types} label={"Types"} field={field}
                                               placeholder={"Choisir un Type"} required={true}/>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="category"
                        render={({field}) => (
                            <FormItem>
                                <SelectElement values={categories} label={"Categories"} field={field}
                                               placeholder={"Choisir une catégories"} required={true}/>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="category"
                        render={({field}) => (
                            <FormItem>
                                <SelectElement values={users} label={"Utilisateurs"} field={field}
                                               placeholder={"Choisir un utilisateur"} required={true}/>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="category"
                        render={({field}) => (
                            <FormItem>
                                <SelectElement values={cycles} label={"Cycles"} field={field}
                                               placeholder={"Choisir un cycle"} required={true}/>
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="isFixed"
                        render={({field}) => (
                            <FormItem>
                                <Toggle label={"Est fixe ?"} value={field.value} name={"isFixed"} />
                            </FormItem>
                        )}
                    />
                </section>
                <Button size={"lg"} variant={"secondary"} className={"mx-auto"} type="submit">Submit</Button>
            </form>
        </Form>
    );
};
