"use client"
import * as React from 'react';
import {usePathname} from "next/navigation";
import {ReactNode} from "react";
import {Dialog, DialogContent} from "@/components/ui/Dialog";

type Props = {
    children?: ReactNode,
    path: string,
    closable?: boolean
};

export const InterceptionModal = ({children, path}: Props) => {

    return (
        <Dialog open={usePathname() === path} onOpenChange={close}>
            <DialogContent className={'dialog-content'}>
                {children}
            </DialogContent>
        </Dialog>
    );
};
