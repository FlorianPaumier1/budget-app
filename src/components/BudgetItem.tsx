// @flow
import * as React from 'react';
import {v4} from "uuid";
import {ItemViewProps} from "@/components/db/query/Items";

type Props = {
    item: ItemViewProps,
    type: string
};
export const BudgetItem = ({item, type}: Props) => {
    return (
        <li className={"border px-4 py-2 bg-white flex flex-col"} key={v4()}>
                            <span className={"flex flex-row justify-between"}>
                                <span>
                                    {item.name}
                                </span>
                                <span className={type == "deposit" ? "text-green-500" : "text-red-500"}>
                                    {type == "deposit" ? "+" : "-"} {(item.amount * item.cycle.multiplicator).toFixed(2)}
                                </span>
                            </span>
            <span>
                                {item.category.name} : {item.date}
            </span>
        </li>
    );
};
