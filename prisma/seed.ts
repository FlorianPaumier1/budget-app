import {Prisma, PrismaClient, User} from "@prisma/client";
import {faker} from "@faker-js/faker";

const prisma = new PrismaClient();

const main = async () => {

    await prisma.item.deleteMany()
    await prisma.user.deleteMany()
    await prisma.category.deleteMany()
    await prisma.type.deleteMany()
    await prisma.cycle.deleteMany()

    const users = [];

    for (let i = 0; i < 2; i++) {
        const user = {
            name: faker.person.firstName(),
        } as User

        const dbUser = await prisma.user.create({
            data: user,
        });

        users.push(dbUser);
    }


    let types: any[] = []

    types.push(await prisma.type.create(
        {
            data: {name: 'expense'}
        }))
    types.push(await prisma.type.create(
        {
            data: {name: 'deposit'}
        }))

    const categoriesName = ['food', 'transport', 'house', 'entertainment', 'health', 'other', "salary"]
    const categories = []

    for (const category of categoriesName) {
        categories.push(await prisma.category.create({
            data: {
                name: category,
                color: faker.internet.color(),
            }
        }))
    }

    const cycleName = [
        {name: 'daily', multiplicator: 1},
        {name: 'weekly', multiplicator: 2},
        {name: 'monthly', multiplicator: 1},
        {name: 'bi-monthly', multiplicator: 2},
        {name: 'yearly', multiplicator: .83}]
    const cycles = []
    for (const string of cycleName) {
        cycles.push(await prisma.cycle.create({
            data: {
                name: string.name,
                multiplicator: string.multiplicator,
            }
        }))
    }

    const items = [{
        userId: users[0].id,
        categoryId: categories[1].id,
        startDate: new Date(2023, 1, 1),
        cycleId: cycles[1].id,
        name: 'Crédit Voiture',
        amount: 120,
        typeId: types[0].id,
    }, {
        userId: users[0].id,
        categoryId: categories[categories.length - 1].id,
        startDate: new Date(2023, 1, 1),
        cycleId: cycles[1].id,
        name: 'Salaire',
        amount: 1600,
        typeId: types[1].id,
    }, {
        userId: users[1].id,
        categoryId: categories[categories.length - 1].id,
        startDate: new Date(2023, 1, 1),
        cycleId: cycles[1].id,
        name: 'Salaire',
        amount: 1600,
        typeId: types[1].id,
    }, {
        userId: users[1].id,
        categoryId: categories[2].id,
        startDate: new Date(2023, 1, 1),
        cycleId: cycles[2].id,
        name: 'Telus',
        amount: 300,
        typeId: types[0].id,
    }, {
        userId: users[1].id,
        categoryId: categories[2].id,
        startDate: new Date(2023, 10, 1),
        endDate: new Date(2023, 12, 1),
        cycleId: cycles[2].id,
        name: 'Taxe',
        amount: 1000,
        typeId: types[0].id,
    }, {
        userId: users[1].id,
        categoryId: categories[2].id,
        startDate: new Date(2023, 10, 1),
        endDate: new Date(2023, 12, 1),
        cycleId: cycles[2].id,
        name: 'Electricité',
        amount: 700,
        typeId: types[0].id,
    }]

    for (const item of items) {
        await prisma.item.create({"data": item})
    }
}


main()
    .then(async () => {
        await prisma.$disconnect();
    })
    .catch(async (e) => {
        // eslint-disable-next-line no-console
        console.error(e);

        await prisma.$disconnect();

        process.exit(1);
    });
